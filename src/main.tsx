import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import './i18n';
import {ChakraProvider, extendTheme} from '@chakra-ui/react'
import {MultiSelectTheme} from 'chakra-multiselect'

const theme = extendTheme({
  components: {
    MultiSelectTheme: MultiSelectTheme
  }
})

ReactDOM.render(
  <React.StrictMode>
      <ChakraProvider theme={theme}>
          <App />
      </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
