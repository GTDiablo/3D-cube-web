import i18n from 'i18next'
import {initReactI18next} from "react-i18next";
import en from './langs/en.json';
import hu from './langs/hu.json';

const resources = {
    en: {
        translation: en,
    },
    hu: {
        translation: hu,
    }
};

const languages = ['hu', 'en'];

i18n.use(initReactI18next).init({
    resources,
    lng: 'hu',
    fallbackLng: 'hu',
    interpolation:{
        escapeValue: false,
    }
});

export default i18n;
