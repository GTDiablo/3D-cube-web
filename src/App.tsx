import {Node} from "./core/types";
import dummyCube from './dummyCube';
import Cube from "./core/cube";
import CubeSerializer from "./core/serlializers";
//
import NodeController from './components/NodeController';
import EngineWindow from "./components/EngineWindow";
import Survey from './components/Survey';
import Watermark from "./components/Watermark";
import Changes from "./components/Changes";
import UsedColors from "./components/UsedColors";
//
import {Fragment, memo, useEffect, useState} from 'react'
import io, {Socket,} from 'socket.io-client';
import {v4 as uuidV4} from 'uuid'
import { Spinner, VStack } from "@chakra-ui/react";
//
import './App.css'
import LocalStorageManager, {LocalStorageItem} from "./libs/LocalStorageManager";

const SOCKET_URL = `${import.meta.env.VITE_HOST}`;

const App = () => {
  const savedUserIdentifier: null | string = LocalStorageManager.get(LocalStorageItem.USER_IDENTIFIER);

  const [userIdentifier] = useState<string>(()=> savedUserIdentifier || uuidV4());
  const [currentNode, setCurrentNode] = useState<Node | null>(null);
  const [cube, setCube] = useState<Cube | null>(null);
  const [socket, setSocket] = useState<Socket | null>(null);
  const [showSurvey, setShowSurvey] = useState<boolean>(!savedUserIdentifier);
  const [changes, setChanges] = useState<Node[]>([]);
  const [usedColors, setUsedColors] = useState<string[]>([]);

  const sendChanges = () => {
    socket?.emit('cube-change-web', { userIdentifier, changes });
    setUsedColors(prevState => {
      const newColors: string[] = changes.map((node: Node)=> node.color).filter((color: string)=> ![...prevState, '#ffffff', '#000000'].includes(color));
      return [...prevState, ...newColors];
    });
    setChanges([]);
  }

  // TODO: Optimize in model
  const setCurrentNodeColor = (node: Node, color: string): void => {
    cube?.setNode(node.point, color);
  }

  useEffect(()=> {
    // Connect to server
    const newSocket = io(SOCKET_URL, {"path": "/socket.io", "forceNew": true, "reconnectionAttempts": 3, "timeout": 2000});
    setSocket(newSocket);

    newSocket?.on('connect', ()=> {
      // Set cube
      setCube(()=> {
        const cubeNodes = dummyCube.nodes;
        const cubeSize = Math.cbrt(cubeNodes.length);

        if(!Number.isInteger(cubeSize)){
          throw new Error('Nodes number is wrong!');
        }

        const rawData: string = JSON.stringify(cubeNodes);

        const cubeSerializer = new CubeSerializer();
        const cube: Cube = cubeSerializer.deserialize(cubeSize, rawData);

        cube.onChange((node: Node, cube: Cube)=> {
          // newSocket?.emit('cube-change-web', node);
          setChanges(prevState => {
            const newState = prevState.filter((n: Node) => !Cube.areNodesEqual(n, node));
            return [node, ...newState];
          });
        });

        return cube;
      });
    })

    newSocket?.on('cube-change-server', (rawNodes)=> {
      console.log('Node change from server:', rawNodes);
      const nodes = rawNodes.map(({point, color}) => {
        const n = new Node();
        n.color = color;
        const {x, y, z} = point;
        n.point = new Point(x,y,z);
        return n;
      })

      nodes.forEach((node: Node) => {
        cube.setNodes(nodes);
      })
    });

    return ()=> {
      newSocket.close();
    }

  }, []);

  const onUsedColorClicked = (color: string) => {
    if(currentNode){
      cube?.setNode(currentNode.point, color);
    }
  }


  const loaded = !!socket && !!cube;

  const renderComponents = () => {
    if(showSurvey){
      return <Survey userIdentifier={userIdentifier} setShowSurvey={setShowSurvey}/>
    }

    if(!loaded){
      return (
          <VStack className="loading-modal">
            <h1>Loading...</h1>
            <Spinner />
          </VStack>
      )
    }

    return (
        <Fragment>
          <EngineWindow
              cube={cube}
              setCurrentNode={setCurrentNode}
          />
          { !!currentNode && <NodeController node={currentNode} setCurrentNodeColor={setCurrentNodeColor} />}
          { changes.length > 0 && <Changes changes={changes} sendChanges={sendChanges}/>}
          <UsedColors colors={usedColors} onColorClicked={onUsedColorClicked}/>
        </Fragment>
    )
  }

  return (
    <div className="App">
      { renderComponents() }
      <Watermark />
    </div>
  )
}

export default memo(App);
