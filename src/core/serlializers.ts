import Cube from "./cube";
import { ICubeSerializer, Node, Point } from "./types";

class CubeSerializer implements ICubeSerializer {
  deserialize(size: number, data: string): Cube {
    const nodesObjects: Node[] = JSON.parse(data);

    if (size < 1 && nodesObjects.length !== Math.pow(size, 3)) {
      throw new Error("Could not deserialize data.");
    }

    const nodes = nodesObjects.map(({ color, point }) => {
      const n = new Node();
      n.color = color;
      const { x, y, z } = point;
      n.point = new Point(x, y, z);
      return n;
    });

    return new Cube(size).setNodes(nodes);
  }

  serialize(cube: Cube): string {
    const nodes: Node[] = cube.getNodes();
    return JSON.stringify(nodes);
  }
}

export default CubeSerializer;
