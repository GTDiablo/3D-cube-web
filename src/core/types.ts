import Cube from "./cube";

export class Point {
  constructor(public x: number, public y: number, public z: number) {}

  public get hash(): string {
    return `${this.x}-${this.y}-${this.z}`;
  }
}

export type Color = string;

export class Node {
  public point!: Point;
  public color!: Color;

  public get hash(): string {
    return this.point.hash;
  }
}

export interface ICubeSerializer {
  serialize: (cube: Cube) => string;
  deserialize: (size: number, data: string) => Cube;
}
