export enum LocalStorageItem {
    USER_IDENTIFIER = 'user-identifier'
}

export interface ILocalStorageManager {
    set(itemKey: LocalStorageItem, data: any): void;
    get(itemKey: LocalStorageItem): null | any;
}

const LocalStorageManager: ILocalStorageManager = {
    set(itemKey, data){
        localStorage.setItem(itemKey, JSON.stringify(data));
    },
    get(itemKey){
        const item = localStorage.getItem(itemKey);

        return !item ? null : JSON.parse(item);
    }
}

export default LocalStorageManager;
