import { SubmitHandler, useForm } from "react-hook-form";
import {
  Box,
  Button,
  Center,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Text,
  VStack,
  useToast,
} from "@chakra-ui/react";
import { Dispatch, FC, memo, SetStateAction, useState } from "react";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import LocalStorageManager, {
  LocalStorageItem,
} from "../../libs/LocalStorageManager";
import { withTranslation, WithTranslation } from "react-i18next";
import LanguageChanger from "../LanguageChanger";
import {MultiSelect} from 'chakra-multiselect';
import i18n from '../../i18n'

type Props = {
  setShowSurvey: Dispatch<SetStateAction<boolean>>;
  userIdentifier: string;
  className?: string;
} & WithTranslation;

type FormInputs = {
  name: string;
  age: number;
  firstTimeLearningToCode: string;
  programingLanguages: string;
};

type UserSurveyDTO = FormInputs & {
  userIdentifier: string;
  language: string;
};

const schema = yup
  .object()
  .shape({
    name: yup.string().required(),
    age: yup.number().required(),
    firstTimeLearningToCode: yup.string(),
    programingLanguages: yup.string(),
  })
  .required();

const Survey: FC<Props> = ({ userIdentifier, setShowSurvey, t }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  });

  const toast = useToast();

  const onSubmit: SubmitHandler<FormInputs> = async (data) => {
    console.log("Form data:", data, userIdentifier);
    const surveyData: UserSurveyDTO = {
      ...data,
      userIdentifier,
      language: i18n.resolvedLanguage
    };
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_HOST}/user-survey`,
        surveyData
      );
      console.log("User survey response", response);
      LocalStorageManager.set(LocalStorageItem.USER_IDENTIFIER, userIdentifier);
      setShowSurvey(false);
    } catch (error) {
      toast({
        title: 'Hiba történt',
        description: 'A szerver jelenleg nem elérhető. Próbáld újra később!',
        status: "error",
        isClosable: true,
        duration: 9000
      })
    }
  };

  const [langs, setLangs] = useState([]);
  const programingLangs = ['Python', 'C++', 'Typescript'];

  return (
    <Flex justify="center" h="100vh" w="100vw" align="center">
      <Center w="100%">
        <VStack spacing="10" alignItems="start">
        <LanguageChanger />

          <Box maxW="32rem" textAlign="left">
            <Heading mb={4}>{t("survey.title")}</Heading>
            <Text fontSize="xl">{t("survey.description")}</Text>
          </Box>
          <Box minW="500">
            <form onSubmit={handleSubmit(onSubmit)}>
              <VStack spacing="7">
                <FormControl isInvalid={!!errors.name} isRequired>
                  <FormLabel htmlFor="name">{t("survey.form.name")}</FormLabel>
                  <Input
                    id="name"
                    placeholder="Zsolt Boda"
                    {...register("name")}
                  />
                  <FormErrorMessage>
                    {errors.name && errors.name.message}
                  </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={!!errors.age} isRequired>
                  <FormLabel htmlFor="name">{t("survey.form.age")}</FormLabel>
                  <Input id="age" placeholder="22" {...register("age")} />
                  <FormErrorMessage>
                    {errors.age && errors.age.message}
                  </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={!!errors.firstTimeLearningToCode}>
                  <FormLabel htmlFor="firstTimeLearningToCode">
                    {t("survey.form.firstTimePrograming")}
                  </FormLabel>
                  <Input
                    id="firstTimeLearningToCode"
                    placeholder="Általános iskolában..."
                    {...register("firstTimeLearningToCode")}
                  />
                  <FormErrorMessage>
                    {errors.firstTimeLearningToCode &&
                      errors.firstTimeLearningToCode.message}
                  </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={!!errors.programingLanguages}>
                  <FormLabel htmlFor="programingLanguages">
                    {t("survey.form.languages")}
                  </FormLabel>
                  <Input
                    id="programingLanguages"
                    placeholder="Python, C/C++, Java..."
                    {...register("programingLanguages")}
                  />
                  <FormErrorMessage>
                    {errors.programingLanguages &&
                      errors.programingLanguages.message}
                  </FormErrorMessage>
                </FormControl>
                {/* <MultiSelect
                  options={programingLangs}
                  value={langs}
                  label={t("survey.form.languages")}
                  onChange={setLangs}
                /> */}
                <Button
                  isFullWidth
                  mt={4}
                  colorScheme="teal"
                  isLoading={isSubmitting}
                  type="submit"
                  size="lg"
                >
                  {t("survey.form.button")}
                </Button>
              </VStack>
            </form>
          </Box>
        </VStack>
      </Center>
    </Flex>
  );
};
export default withTranslation()(memo(Survey));
