import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import React, { memo, useState, useEffect } from "react";
import i18n from '../../i18n.ts'

const LanguageChanger = () => {
  const languages = [
    {
      icon: '/flags/hun.svg',
      label: 'Magyar',
      value: 'hun',
    },
    {
      icon: '/flags/en.svg',
      label: 'English',
      value: 'en',
    }
  ]
  const [currentLang, setCurrentLang] = useState(languages[0]);

  useEffect(() => {
    i18n.changeLanguage(currentLang.value);
  }, [currentLang]);

  console.log('LANG', i18n);

  return (
    <Menu>
      <MenuButton as={Button}>{currentLang.label}</MenuButton>
      <MenuList>
        {languages.map(({icon, label, value}) => (
          <MenuItem key={value} onClick={() => setCurrentLang({icon, label, value})}>
            <span>{label}</span>
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export default memo(LanguageChanger);
