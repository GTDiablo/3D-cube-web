import React, {FC, memo} from 'react';
import { Node } from "../../core/types";
import styles from './Changes.module.scss'
import classNames from "classnames";
import {withTranslation, WithTranslation} from "react-i18next";

type Props = {
    changes: Node[];
    sendChanges: () => void;
    className?: string;
} & WithTranslation;

const Changes: FC<Props> = ({changes, sendChanges, className='', t}) => {

    return (
        <div className={classNames(styles['Changes'], className)}>
            <button
                className={styles['Changes__button']}
                onClick={sendChanges}>
                {t('app.sendChanges')}
            </button>
            <div className={styles['Changes__changes']}>
                {changes.map((node: Node, index: number)=> (
                    <div
                        className={styles['Changes__change-item']}
                        key={index}
                    >
                        {Object.entries(node.point).map(([key, value])=> (
                            <div key={key} className={styles["Changes__change-item__point"]}>{key}: {value}</div>
                        ))}
                        <div className={styles['Changes__change-item__color']} style={{backgroundColor: node.color}}/>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default withTranslation()(memo(Changes));

