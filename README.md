#  Boda Zsolt Szakdolgozat projektje: 3D Fény programozás

## Technikai információk és demo
Demo megtekinthető a [https://szakdoga.zsoltboda.com/](https://szakdoga.zsoltboda.com/) címen.

### Project letöltése és indítása lokáslis környezetben:
Project klónozása gitlab-ről:
```shell
git clone https://gitlab.com/GTDiablo/3D-cube-server.git
```
Projekt mappájába navigálás:
```shell
cd 3D-cube-server
```
Függőségek telepítése
```shell
yarn install
```

### Project indítása
Lokális development build + éles szerver:
```shell
yarn staging
```
Lokális development build + lokális szerver:
```shell
yarn dev
```
> **Fontos:** Ilyenkor lokálisan kell futtatni a 3D cube server projektet is! Külön a szerver a [https://gitlab.com/GTDiablo/3D-cube-server](https://gitlab.com/GTDiablo/3D-cube-server) címent elérhető.
